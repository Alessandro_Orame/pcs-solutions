from math import pi, sqrt


class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y


class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.center = center
        self.a = a
        self.b = b

    def area(self):
        return pi*self.a*self.b


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        super(Circle, self).__init__(center, radius, radius)

    def area(self):
        return super().area()


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3

    def area(self):
        return 0.5*((self.p1.y*(self.p2.x-self.p3.x))+(self.p2.y*(self.p3.x-self.p1.x))+(self.p3.y*(self.p1.x-self.p2.x)))


class TriangleEquilateral(IPolygon):
    def __init__(self, p1: Point, edge: int):
        self.p1 = p1
        self.edge = edge

    def area(self):
        return sqrt(3)*0.25*(self.edge*self.edge)


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.p4 = p4

    def area(self):
        return 0.5*abs((self.p1.x*self.p2.y)+(self.p2.x*self.p3.y)+(self.p3.x*self.p4.y)+(self.p4.x*self.p1.y)-(self.p2.x*self.p1.y)-(self.p3.x*self.p2.y)-(self.p4.x*self.p3.y)-(self.p1.x*self.p4.y))


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        self.p1 = p1
        self.p2 = p2
        self.p4 = p4
        super(Parallelogram, self).__init__(p1, p2, Point(self.p2.x-self.p1.x+self.p4.x, self.p2.y-self.p1.y+self.p4.y), p4)

    def area(self):
        return super().area()


class Rectangle(IPolygon):
    def __init__(self, p1: Point, base: int, height: int):
        self.p1 = p1
        self.base = base
        self.height = height

    def area(self):
        return self.base*self.height


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super(Square, self).__init__(p1, edge, edge)

    def area(self):
        return super().area()