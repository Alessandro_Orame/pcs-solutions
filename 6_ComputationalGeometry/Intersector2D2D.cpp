#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    //definiamo le tolleranze rispetto all'intersezione e rispetto alla condizione di parallelismo
    toleranceIntersection = 1.0E-7;
    toleranceParallelism  = 1.0E-5;

    matrixNormalVector.setZero(); // matrixNormalVector ha come prima e seconda riga i vettori normali al piano
                                  //e come terza riga ha il loro prodotto vettoriale,
                                  //ossia un vettore tangente alla retta individuata dall'intersezione

    rightHandSide.setZero();  //rightHandSide è un vettore che contiene i parametri di traslazione del piano
}

// ***************************************************************************

void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNormalVector.row(0) = planeNormal;
    rightHandSide(0) = planeTranslation;
}

// ***************************************************************************

void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNormalVector.row(1) = planeNormal;
    rightHandSide(1) = planeTranslation;
  return ;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    tangentLine = (matrixNormalVector.row(0)).cross(matrixNormalVector.row(1));

    // verifichiamo se N1 è parallelo a N2
    if (tangentLine.squaredNorm() <= toleranceParallelism * toleranceParallelism) {
        //I PIANI SONO PARALLELI

        // controlliamo se sono coincidenti
        double ratio = 0;
        if (matrixNormalVector(0, 0) >= 1.0E-14)  //se non è = 0:
            ratio = matrixNormalVector(0, 1) / matrixNormalVector(0, 0);
        else if (matrixNormalVector(0, 1) >= 1.0E-14)   //se non è = 0:
            ratio = matrixNormalVector(1, 1) / matrixNormalVector(0, 1);
        else
            ratio = matrixNormalVector(1, 2) / matrixNormalVector(0, 2);

        if (abs(rightHandSide(1) - rightHandSide(0) * ratio) <= 1.0E-16)
        {
            intersectionType = Coplanar;
            return false;
        }
        else
        {
            intersectionType = NoInteresection;
            return false;
        }
    }

    pointLine = matrixNormalVector.colPivHouseholderQr().solve(rightHandSide);
    intersectionType = LineIntersection;
    return true;
}
