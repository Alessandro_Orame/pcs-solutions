#include "Intersector2D1D.h"

// ***************************************************************************

//definiamo le tolleranze per l'intersezione e per la condizione di parallelismo
Intersector2D1D::Intersector2D1D()
{
    toleranceIntersection = 1.0E-7;
    toleranceParallelism  = 1.0E-7;
}

Vector3d Intersector2D1D::IntersectionPoint() {

    // nel caso in cui non ci sia intersezione o non sia stata ancora calcolata
    if (IntersectionType() != PointIntersection)
        throw runtime_error("There is no intersection");

    //restituiamo il punto di intersezione
    return *lineOriginPointer + intersectionParametricCoordinate*(*lineTangentPointer);
}

// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer = &planeNormal;  //indica il vettore normale al piano
    planeTranslationPointer = &planeTranslation; //indica il parametro di traslazione (d) del piano
  return;
}
// ***************************************************************************

//
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer = &lineOrigin;   //origine della retta
    lineTangentPointer = &lineTangent; //vettore tangente alla retta
  return;
}
// ***************************************************************************

//CALCOLIAMO LE INTERSEZIONI, L'OUTPUT SARà TRUE SE C'è INTERSEZIONE TRA RETTA E PIANO
bool Intersector2D1D::ComputeIntersection()
{
    //calcoliamo il prodotto scalare tra la normale al piano ed il vettore tangente alla retta
    double normalDotTangent = (*planeNormalPointer).dot(*lineTangentPointer);
    double normalDotDifferencePlaneLine = *planeTranslationPointer - (*planeNormalPointer).dot(*lineOriginPointer);

    //controlliamo se il piano normale n è perpendicolare al vettore tangente (retta e piano paralleli)
    if (-toleranceParallelism <= normalDotTangent && normalDotTangent <= toleranceParallelism) {

        // controlliano se la retta è contenuta nel piano
        if (-toleranceParallelism <= normalDotDifferencePlaneLine && normalDotDifferencePlaneLine <= toleranceParallelism) {

            //La retta è contenuta nel piano: ritorniamo "false"
            intersectionType = Coplanar;
            return false;
        }

        else {
            //Non ci sono intersezioni tra retta e piano: ritorniamo "false"
            intersectionType = NoInteresection;
            return false;
        }
    }

    //Se retta e piano non sono paralleli, c'è intersezione
    intersectionType = PointIntersection;

    //calcoliamo così il valore dell'ascissa curvilinea
    double s = normalDotDifferencePlaneLine / normalDotTangent;

    intersectionParametricCoordinate = s; //poniamo infine uguale a s il valore della coordinata parametrica di intersezione

    return true;
}
