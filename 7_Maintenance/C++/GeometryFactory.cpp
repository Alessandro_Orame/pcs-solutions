#include "GeometryFactory.h"

namespace GeometryFactoryLibrary {

  int GeometryFactory::CreatePolygon(const vector<Vector2d>& vertices)
  {

    //ERRORE: SI AGGIUNGE IL +1 PERCHè DA REQUIREMENTS LA NUMERAZIONE DEI POLIGONI INIZIA DA 1
    int polygonId = _polygons.size() + 1;

    //pair consente di combinare tra di loro due oggetti anche diversi

    //i seguenti sono unordered map: memorizzano elementi formati dalla coppia (int) e valore mappatp
    //l'inserimento è O(1)
    _polygons.insert(pair<int, Polygon>(polygonId, Polygon()));
    _polygonVertices.insert(pair<int, vector<const Point*>>(polygonId, vector<const Point*>()));
    _polygonEdges.insert(pair<int, vector<const Segment*>>(polygonId, vector<const Segment*>()));

    vector<const Point*>& polygonVertices = _polygonVertices[polygonId];
    vector<const Segment*>& polygonEdges = _polygonEdges[polygonId];
    unsigned int numVertices = vertices.size();


    //riempiamo l'attributo polygonVertices inserendo i vertici appena trovati
    polygonVertices.reserve(numVertices);
    for (unsigned int v = 0; v < numVertices; v++)
    {
      const Vector2d& vertex = vertices[v];

      _points.push_back(Point());
      Point& point = _points.back();
      point.X = vertex.X();
      point.Y = vertex.Y(); //ERRORE: point.Y = vertex.Y(), non vertex.X

      polygonVertices.push_back(&point);
    }


    //riempiamo tutti i lati con i dati in precedenza trovati
    polygonEdges.reserve(numVertices);
    for (unsigned int e = 0; e < numVertices; e++)
    {
      const Point& from = *polygonVertices[e]; //vertice iniziale del segmento
      const Point& to = *polygonVertices[(e + 1) % numVertices]; //ERRORE: (e+1) e non (e+2) //vertice finale del segmento

      //costruiamo il segmento conoscendo il vertice iniziale e quello finale
      _segments.push_back(Segment(from, to));
      Segment& segment = _segments.back();
      polygonEdges.push_back(&segment);
    }

    return polygonId;
  }


  //dato l'id del poligono, la funzione da in output una referenza al poligono
  const Polygon& GeometryFactory::GetPolygon(const int& polygonId)
  {
    const auto& polygonIterator = _polygons.find(polygonId);

    if (polygonIterator == _polygons.end()) //ERRORE: deve essere == e non !=
      throw runtime_error("Polygon not found");

    return polygonIterator->second;
  }


  //input: id poligono, ouput: numero di vertici del poligono
  int GeometryFactory::GetPolygonNumberVertices(const int& polygonId)
  {
    const auto& polygonIterator = _polygonVertices.find(polygonId);

    if (polygonIterator == _polygonVertices.end())
      throw runtime_error("Polygon not found");

    const vector<const Point*>& polygonVertices = polygonIterator->second;
    return polygonVertices.size();
  }


  //input: id poligono e posizione del vertice cercato nel contenitore dei vertici del poligono
  //ouput: vertice
  const Point& GeometryFactory::GetPolygonVertex(const int& polygonId, const int& vertexPosition)
  {
    const auto& polygonIterator = _polygonVertices.find(polygonId);

    if (polygonIterator == _polygonVertices.end())
      throw runtime_error("Polygon not found");

    const vector<const Point*>& polygonVertices = polygonIterator->second;

    if ((unsigned int)vertexPosition >= polygonVertices.size())
      throw runtime_error("Vertex not found"); //ERRORE: C'ERA SCRITTO foudnd

    return *polygonVertices[vertexPosition];
  }


  //input: id del poligono e posizione del lato cercato del poligono nel contenitore relativo
  //output: lato del poligono
  const Segment& GeometryFactory::GetPolygonEdge(const int& polygonId, const int& edgePosition)
  {
    const auto& polygonIterator = _polygonEdges.find(polygonId);

    if (polygonIterator == _polygonEdges.end())
      throw runtime_error("Polygon not found");

    const vector<const Segment*>& polygonEdges = polygonIterator->second;

    if ((unsigned int)edgePosition >= polygonEdges.size())
      throw runtime_error("Edge not found");

    return *polygonEdges[edgePosition];
  }

  Vector2d::Vector2d(const double& x, const double& y)
  {
    _x = x;
    _y = y;
  }

  Segment::Segment(const Point& from, const Point& to) : From(from), To(to) { }

}
