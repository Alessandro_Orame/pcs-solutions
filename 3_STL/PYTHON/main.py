class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.name = name
        self.price = price
        self.description = description


class Pizza:
    def __init__(self, name: str):
        self.name = name
        self.ingredients = []

    def addIngredient(self, ingredient: Ingredient):
        self.ingredients.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.ingredients)

    def computePrice(self) -> int:
        prezzopizza = 0
        for ingredient in self.ingredients:
            prezzopizza += ingredient.price
        return prezzopizza


class Order:
    def __init__(self):
        self.pizze = []

    def getPizza(self, position: int) -> Pizza:
        if position > len(self.pizze):
            raise ValueError("Position passed is wrong")
        return self.pizze[position-1]

    def initializeOrder(self, numPizzas: int):
        if numPizzas == 0:
            raise ValueError("Empty order")

    def addPizza(self, pizza: Pizza):
        self.pizze.append(pizza)

    def numPizzas(self) -> int:
        return len(self.pizze)

    def computeTotal(self) -> int:
        prezzoTot = 0
        for pizza in self.pizze:
            prezzoTot += pizza.computePrice()
        return prezzoTot


class Pizzeria:

    def __init__(self):
        self.list = []
        self.menupizze = []
        self.ordini = []

    def addIngredient(self, name: str, description: str, price: int):
        indice: int = 0
        for ingredient in self.list:
            if ingredient.name == name:
                raise ValueError("Ingredient already inserted")
            if ingredient.name > name:
                indice = self.list.index(ingredient)
        self.list.insert(indice, Ingredient(name, price, description))
        if len(self.list) == 0:
            self.list.append(Ingredient(name, price, description))

    def findIngredient(self, name: str) -> Ingredient:
        for ingredient in self.list:
            if ingredient.name == name:
                return ingredient
        raise ValueError("Ingredient not found")

    def addPizza(self, name: str, ingredients: []):
        nuovapizza = Pizza(name)
        for pizza in self.menupizze:
            if pizza.name == name:
                raise ValueError("Pizza already inserted")
        for ingredient in ingredients:
            nuovapizza.addIngredient(self.findIngredient(ingredient))
        self.menupizze.append(nuovapizza)

    def findPizza(self, name: str) -> Pizza:
        for pizza in self.menupizze:
            if pizza.name == name:
                return pizza
        raise ValueError("Pizza not found")

    def createOrder(self, pizzas: []) -> int:
        ordine = Order()
        ordine.initializeOrder(len(pizzas))
        for pizza in pizzas:
            ordine.addPizza(self.findPizza(pizza))
        numeroOrdine = 1000 + len(self.ordini)
        self.ordini.append(ordine)
        return numeroOrdine

    def findOrder(self, numOrder: int) -> Order:
        if len(self.ordini) + 1000 < numOrder + 1:
            raise ValueError("Order not found")
        return self.ordini[numOrder - 1000]

    def getReceipt(self, numOrder: int) -> str:
        Receipt: str = ""
        ordine = self.findOrder(numOrder)
        for pizza in ordine.pizze:
            Receipt += "- " + pizza.name + ", " + str(pizza.computePrice()) + " euro\n"
        Receipt += "  TOTAL: " + str(ordine.computeTotal()) + " euro\n"
        return Receipt

    def listIngredients(self) -> str:
        lista: str = ""
        for ingredient in self.list:
            lista += ingredient.name + " - '" + ingredient.description + "': " + str(ingredient.price) + " euro\n"
        return lista

    def menu(self) -> str:
        Menu: str = ""
        for pizza in self.menupizze:
            Menu += pizza.name + " (" + str(pizza.numIngredients()) + " ingredients): " + \
                    str(pizza.computePrice()) + " euro\n"
        return Menu
