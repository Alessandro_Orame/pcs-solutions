#include "Pizzeria.h"
#include <iostream>
#include <vector>
#include <list>
#include <stack>
#include <queue>
#include <unordered_set>
#include <algorithm>

namespace PizzeriaLibrary {


Ingredient::Ingredient(string Nome, string Descrizione, int Prezzo){
    Name = Nome;
    Price = Prezzo;
    Description = Descrizione;
}


Pizza::Pizza(const string Nome, vector <Ingredient> ingredienti){
    Name = Nome;
    Ingredients = ingredienti;
}


Order::Order (const int NumOrder, vector <Pizza> pizze){
     numOrder = NumOrder;
     VectorPizza = pizze;
}



void Pizza::AddIngredient(const Ingredient& ingredient) {Ingredients.push_back(ingredient);}


int Pizza::NumIngredients() const {return Ingredients.size();}


int Pizza::ComputePrice() const {
    int PrezzoPizza = 0;

    for (unsigned int i=0; i<Ingredients.size(); i++)
    {
        PrezzoPizza += Ingredients.at(i).Price;
    }

    return PrezzoPizza;
}


void Order::InitializeOrder(int numPizzas) {VectorPizza.reserve(numPizzas);}


void Order::AddPizza(const Pizza &pizza) {VectorPizza.push_back(pizza);}


const Pizza& Order::GetPizza(const int &position) const {return VectorPizza[position];}


int Order::NumPizzas() const{
    return VectorPizza.size();

};


int Order::ComputeTotal() const {
    int PrezzoOrdine=0;
    for (unsigned int i=0; i<VectorPizza.size(); i++)
        PrezzoOrdine += VectorPizza.at(i).ComputePrice();
    return PrezzoOrdine;
};


void Pizzeria::AddIngredient(const string& name, const string& description, const int& price){

    vector<Ingredient>::iterator it;
    for (it=List.begin(); it!=List.end(); it++){
        if ((*it).Name == name)
            throw runtime_error ("Ingredient already inserted");
        if(name < (*it).Name)
            break;
    }
    List.insert(it, Ingredient(name, description, price));
}


const Ingredient& Pizzeria::FindIngredient(const string& name) const {

    int boolean = false, position =0;
    for (unsigned int i=0; i<List.size(); i++)
        if(List[i].Name == name){
            position = i;
            boolean = true;
    }
    if (boolean == false)
        throw runtime_error("Ingredient not found");
    else
        return List[position];
}


void Pizzeria::AddPizza(const string& name, const vector<string>& ingredients) {

    for (list<Pizza>::iterator it = MenuPizze.begin(); it != MenuPizze.end(); it++){
        if (name == (*it).Name){
            throw runtime_error("Pizza already inserted");
        }
    }
    vector <Ingredient> ingredienti;
    for (unsigned int j=0; j<ingredients.size(); j++)
        ingredienti.push_back(FindIngredient(ingredients[j]));
    MenuPizze.push_front(Pizza(name, ingredienti));
}


const Pizza& Pizzeria::FindPizza(const string& name) const {

    list<Pizza>::const_iterator temp;
    int boolean = false;
    for (list<Pizza>::const_iterator it = MenuPizze.cbegin(); it != MenuPizze.cend(); it++){
        if(name == (*it).Name){
            temp = it;
            boolean = true;
        }
    }
    if (boolean == false)
        throw runtime_error("Pizza not found");
    else
        return (*temp);
}


int Pizzeria::CreateOrder(const vector<string>& pizzas) {

    if(pizzas.size()==0)
        throw runtime_error("Empty order");

    vector <Pizza> pizze;
    for (unsigned int i=0; i<pizzas.size(); i++){
        pizze.push_back(FindPizza(pizzas[i]));
    }
    int numOrder = 1000 + Ordini.size();
    Ordini.push_back(Order(numOrder, pizze));
    return numOrder;
}


const Order& Pizzeria::FindOrder(const int& numOrder) const {

    int boolean = false, position = 0;
    for (unsigned int i=0; i<Ordini.size(); i++)
        if(Ordini[i].numOrder==numOrder){
            boolean = true;
            position = i;
        }
    if (boolean == false)
        throw runtime_error("Order not found");
    else
        return Ordini[position];
}


string Pizzeria::GetReceipt(const int& numOrder) const {

        string Receipt = "";
        Order ordine = FindOrder(numOrder);
        for (unsigned int i=0; i< ordine.VectorPizza.size(); i++)
            Receipt += "- " + ordine.VectorPizza[i].Name + ", " + to_string(ordine.VectorPizza.at(i).ComputePrice()) + " euro" + "\n";
        Receipt +=  "  TOTAL: " + to_string(ordine.ComputeTotal()) + " euro" + "\n";
        return Receipt;
}


string Pizzeria::ListIngredients() const {

    string lista="";
    for(vector <Ingredient>::const_iterator it = List.cbegin(); it != List.cend(); it++)
        lista += (*it).Name + " - '" + (*it).Description + "': " + to_string((*it).Price) + " euro" + "\n";
    return lista;
}


string Pizzeria::Menu() const {

    string Menu="";
    for (list<Pizza>::const_iterator it = MenuPizze.cbegin(); it != MenuPizze.cend(); it++)
        Menu += (*it).Name + " (" + to_string((*it).NumIngredients()) + " ingredients): " + to_string((*it).ComputePrice()) + " euro" + "\n";
    return Menu;
}

}













