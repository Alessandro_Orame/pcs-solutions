#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <list>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;

  public:
      Ingredient(string Nome, string  Descrizione, int Prezzo);
  };

  class Pizza {
    public:
      string Name;
      vector <Ingredient> Ingredients;

 public:
      Pizza(const string Nome,
            vector <Ingredient> ingredienti);

      void AddIngredient(const Ingredient& ingredient);
      int NumIngredients() const;
      int ComputePrice() const ;
  };

  class Order {
  public:
      int numOrder;
      vector <Pizza> VectorPizza;

    public:
      Order (const int NumOrder,
             vector <Pizza> pizze);

      void InitializeOrder(int numPizzas);
      void AddPizza(const Pizza& pizza);
      const Pizza& GetPizza(const int& position) const;
      int NumPizzas() const;
      int ComputeTotal() const;
  };

  class Pizzeria {

  public:
      vector <Ingredient> List;
      list <Pizza> MenuPizze;
      vector <Order> Ordini;

    public:
      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;

      /*const Ingredient& MyFunction(const Ingredient& ingredient1, const Ingredient& ingredient2);*/

  };
};

#endif // PIZZERIA_H
