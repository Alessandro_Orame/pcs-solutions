# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;


//COSTRUTTORE BusStation
BusStation::BusStation(const string &busFilePath) {
    _busFilePath = busFilePath;
}


//CARICHIAMO I DATI RELATIVI ALLA BUS STATION RICAVANDOLI DA UN FILE ESTERNO
void BusStation::Load() {


    //resettiamo le possibili informazioni già contenute negli attributi della classe
    //per poter inserire i nuovi dati ricavati dal file
    _numberBuses = 0;
    _buses.clear();


    //apertura file e verifica di eventuali errori durante l'operazione
    ifstream file;
    file.open(_busFilePath);
    if (file.fail())
        throw runtime_error("Something goes wrong");

     try {

        //LEGGIAMO IL NUMERO DI BUS DA FILE E LO SALVIAMO NELL'ATTRIBUTO CORRISPONDENTE
        string stringa;
        getline(file, stringa); //QUESTA RIGA DEL FILE è DA SCARTARE
        getline(file, stringa); //contiene il numero di bus della Bus Station
        istringstream numeroBus;
        numeroBus.str(stringa);
        numeroBus >> _numberBuses;

        //LEGGIAMO DA FILE L'ID DEI BUS E IL COSTO ORARIO DELLA BENZINA DI CIASCUNO
        //E SALVIAMO TUTTO NEGLI ATTRIBUTI CORRISPONDENTI
        int id, fuelCost;
        getline(file, stringa);    //QUESTA RIGA DEL FILE è DA SCARTARE
        getline(file, stringa);    //QUESTA RIGA DEL FILE è DA SCARTARE
        for (int i = 0; i < _numberBuses; i++)
        {
            getline(file, stringa);
            istringstream dati;
            dati.str(stringa);
            dati >> id >> fuelCost;
            _buses.push_back(Bus{id, fuelCost});
        }
    }

    catch (exception) {
        //se qualcosa andasse storto nella lettura da file,
        //resettiamo gli attributi della classe e lanciamo un messaggio di errore
        _numberBuses = 0;
        _buses.clear();
    throw runtime_error("Something goes wrong");
    }

}


//CERCHIAMO IL BUS NELLA BUS STATION ATTRAVERSO IL SUO ID E LO DIAMO IN OUTPUT
const Bus &BusStation::GetBus(const int &idBus) const {

    //se non esiste alcun id corrispondente al bus che stiamo cercando, lanciamo un messaggio di errore
    if (idBus <= 0 || idBus > _numberBuses)
        throw runtime_error("Bus " + to_string(idBus) + " does not exists");

    return _buses[idBus - 1];
}


//metodo che resetta tutti gli attributi della classe MapData
//ne faremo uso in seguito
void MapData::Reset()
{
    _numberRoutes = 0;
    _numberStreets = 0;
    _numberBusStops = 0;
    _routes.clear();
    _streets.clear();
    _busStops.clear();
    _streetsFrom.clear();
    _streetsTo.clear();
    _routeStreets.clear();
}


//CARICHIAMO I DATI RELATIVI ALLA MapData RICAVANDOLI DA UN FILE ESTERNO
void MapData::Load() {

    Reset(); //usiamo la funzione qui sopra definita per resettare tutti gli attributi della classe
             //prima di inserire i nuovi dati letti da file

    //apertura file e verifica di eventuali errori durante l'operazione
    ifstream file;
    file.open(_mapFilePath);
    if (file.fail())
        throw runtime_error("Something goes wrong");

    try {

        //LEGGIAMO IL NUMERO DI BUS DA FILE E LO SALVIAMO NELL'ATTRIBUTO CORRISPONDENTE
        string stringa;
        getline(file, stringa);   //QUESTA RIGA DEL FILE è DA SCARTARE
        getline(file, stringa);   //contiene il numero di BusStop della MapData

        istringstream numeroBusStop;
        numeroBusStop.str(stringa);
        numeroBusStop >> _numberBusStops;


        //CARICHIAMO ORA DA FILE LE VERE E PROPRIE FERMATE DEI BUS
        getline(file, stringa);    // QUESTA RIGA DEL FILE è DA SCARTARE

        for (int i = 0; i < _numberBusStops; i++)
        {
            int idBusStop, latitude, longitude;
            string name;

            getline(file, stringa);
            istringstream busStopConverter;
            busStopConverter.str(stringa);
            busStopConverter >> idBusStop >> name >> latitude >> longitude; //dal file ricaviamo i 4 attributi della classe BusStop

            //COSTRUTTORE DI DEFAULT:
            _busStops.push_back(BusStop{idBusStop, latitude, longitude, name});
        }


        //LEGGIAMO DAL FILE IL NUMERO DI STRADE
        getline(file, stringa);    //QEUSTA RIGA DEL FILE è DA SCARTARE
        getline(file, stringa);    //contiene il numero di strade

        istringstream numeroStrade;
        numeroStrade.str(stringa);
        numeroStrade >> _numberStreets;


        //LEGGIAMO DAl FILE LE STRADE VERE E PRORPIE
        getline(file, stringa);    //QUESTA RIGA DEL FILE è DA SCARTARE
        for (int i = 0; i < _numberStreets; i++)
        {
            int idStreet, from, to, travelTime;
            getline(file, stringa);
            istringstream streetConverter;
            streetConverter.str(stringa);
            streetConverter >> idStreet >> from >> to >> travelTime;

            _streets.push_back(Street{idStreet, travelTime}); //COSTRUTTORE DI DEFAULT DELLA CLASSE Street
            _streetsFrom.push_back(from); //aggiungiamo al vettore streetFrom la fermata che rappresenta l'inizio della strada i-esima
            _streetsTo.push_back(to);     //aggiungiamo al vettore streetTo la fermata che rappresenta la fine della strada i-esima
        }


        //LEGGIAMO DA FILE IL NUMERO DI PERCORSI
        getline(file, stringa);    //QUESTA RIGA DEL FILE è DA SCARTARE
        getline(file, stringa);

        istringstream numberRoutesConverter;
        numberRoutesConverter.str(stringa);
        numberRoutesConverter >> _numberRoutes;


        //LEGGIAMO DAL FILE I VERI E PORPRI PERCORSI
        int id, streetNumber;
        getline(file, stringa);    //QUESTA RIGA DEL FILE è DA SCARTARE

        for (int i=0; i<_numberRoutes; i++)
        {
            getline(file, stringa);
            istringstream Routes;
            Routes.str(stringa);
            Routes >> id >> streetNumber;

            _routes.push_back(Route{id, streetNumber});

            // routeStreets is vector<vector<int>> so we add another vector and fill it
            // each vector<int> shows the route with all the streets
            _routeStreets.push_back(vector<int>());
            for (int n = 0; n < streetNumber; n++)
            {
                int idStreet;
                Routes >> idStreet;
                _routeStreets[i].push_back(idStreet);
            }
        }

    }

    catch (exception) {
        //se qualcosa andasse storto nella lettura da file,
        //resettiamo gli attributi della classe e lanciamo un messaggio di errore
        Reset();
        throw runtime_error("Something goes wrong");
    }
}


//CERCHIAMO UNA STRADA NELLA MAPPA CONOSCENDONE LA POSIZIONE SU UN PERCORSO
const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const {

    Route route = GetRoute(idRoute); //salviamo nella variabile route il percorso (trovato con la GetRoute)
                                     //su cui si trova la strada che cerchiamo

    //lanciamo un messaggio di errore nel caso non sia possibile trovare la strada sul percorso selezionato
    if (streetPosition < 0 || streetPosition > route.NumberStreets)
        throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");


    //salviamo in idStreet l'id della strada che si trova alla posizione data streetPosition
    //scegliendola nel percorso che si trova in posizione idRoute-1 nel vettor di percorsi
    int idStreet = _routeStreets[idRoute - 1][streetPosition];


    //possiamo ora dare in output la strada conoscendone l'id
    return _streets[idStreet - 1];
}


//RESTITUIAMO IL PERCORSO CERCATO CONOSCENDONE L'ID
const Route &MapData::GetRoute(const int &idRoute) const {

    //lanciamo un messaggio di errore nel caso non sia possibile trovare il percorso
    if(idRoute <= 0 || idRoute > _numberRoutes)
        throw runtime_error("Route " + to_string(idRoute) + " does not exists");

    return _routes[idRoute - 1];
}


//RESTITUIAMO LA STRADA CERCATA CONOSCENDONE L'ID
const Street &MapData::GetStreet(const int &idStreet) const {

    //lanciamo un messaggio di errore nel caso non sia possibile trovare la strada
    if(idStreet <= 0 || idStreet > _numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    return _streets[idStreet - 1];
}


//RESTITUIAMO LA FERMATA DEL BUS DA CUI INIZIA LA STRADA DATA IN INPUT
const BusStop &MapData::GetStreetFrom(const int &idStreet) const {

    //lanciamo un messaggio di errore nel caso non sia possibile trovare la fermata
    if(idStreet <= 0 || idStreet > _numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int idBus = _streetsFrom[idStreet - 1];
    return _busStops[idBus - 1];
}


//RESTITUIAMO LA FERMATA DEL BUS IN CUI FINISCE LA STRADA DATA IN INPUT
const BusStop &MapData::GetStreetTo(const int &idStreet) const {

    //lanciamo un messaggio di errore nel caso non sia possibile trovare la fermata
    if(idStreet <= 0 || idStreet > _numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int idBus = _streetsTo[idStreet - 1];
    return _busStops[idBus - 1];
}


//RESTITUIAMO LA FERMATA DEL BUS CONOSCENDONE L'ID
const BusStop &MapData::GetBusStop(const int &idBusStop) const {

    //lanciamo un messaggio di errore nel caso non sia possibile trovare la fermata
    if(idBusStop <= 0 || idBusStop > _numberBusStops)
        throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");

    return _busStops[idBusStop - 1];
}


//RESTITUIAMO IL VALORE DEL TEMPO TOTALE IMPIEGATO A PERCORRERE UN PERCORSO
int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const{

    int tempoTotale= 0;
    const Route& route = _mapData.GetRoute(idRoute); //salviamo nella variabile route il percorso trovato con la GetRoute


    //per ogni strada nel percorso, sommiamo al tempo totale il tempo impiegato a percorrere tale strada
    for (int i=0; i<route.NumberStreets; i++)
    {
        const Street& street = _mapData.GetRouteStreet(idRoute, i);
        tempoTotale += street.TravelTime;
    }

    return tempoTotale;
}


//RESTITUIAMO IL VALORE DEL COSTO TOTALE DI UN PERCORSO
int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const {

    double costoTotale = 0.0;
    const Route& route = _mapData.GetRoute(idRoute);  //salviamo nella variabile route il percorso trovato con la GetRoute
    const Bus& bus = _busStation.GetBus(idBus);  //salviamo nella variabile in questione il bus trovato con la GetBus


    //sommiamo al costo totale il costo di ciascuna strada nel percorso
    for (int i=0; i<route.NumberStreets; i++)
    {
        const Street& street = _mapData.GetRouteStreet(idRoute, i);
        costoTotale += (double)street.TravelTime * (double)bus.FuelCost * ((double)BusAverageSpeed / 3600.0);
    }

    return (int)costoTotale;
}


//RESTITUIAMO UNA STRINGA CONTENENTE TUTTE LE INFORMAZIONI RIGUARDO IL PERCORSO CERCATO (TRAMITE L'ID)
string MapViewer::ViewRoute(const int &idRoute) const {

    const Route& route = _mapData.GetRoute(idRoute);//salvo nella variabile route il percorso ricavato con la GetRoute

    ostringstream routeView;  //UTILIZZO UN ostringstream PER CONCATENARE NELLA STRINGA TUTTE LE STRADE CHE COMPONGONO IL PERCORSO
    routeView << idRoute << ": ";

    //concateno una dopo l'altra le informazioni di ciascuna strada nel percorso
    for (int i=0; i<route.NumberStreets; i++)
    {
        const Street& street = _mapData.GetRouteStreet(idRoute, i);
        const BusStop& from = _mapData.GetStreetFrom(street.Id);
        routeView<< from.Name<< " -> ";

        // QUANDO ARRIVIAMO ALLA FINE DEL PERCORSO DOBBIAMO AGGIUNGERE L'ULTIMA STRADA E FERMARCI
        if (i == route.NumberStreets - 1)
        {
            const BusStop& to = _mapData.GetStreetTo(street.Id);
            routeView<< to.Name;  //l'ultima informazione aggiunta è la fermata finale dell'ultima strada del percorso
        }
    }

    return routeView.str();
}


//RESTITUIAMO UNA STRINGA CONTENENTE TUTTE LE INFORMAZIONI RIGUARDO LA STRADA CERCATA (TRAMITE L'ID)
string MapViewer::ViewStreet(const int &idStreet) const {

    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);

    //la stringa conterrà l'id della strada, la fermata iniziale e la fermata finale
    return to_string(idStreet) + ": " + from.Name + " -> " + to.Name;
}


//RESTITUIAMO UNA STRINGA CONTENENTE TUTTE LE INFORMAZIONI RIGUARDO LA FERMATA BUS CERCATA (TRAMITE L'ID)
string MapViewer::ViewBusStop(const int &idBusStop) const {

    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

    //la stringa conterrà il nome della fermata e le coordinate relative alla sua posizione
    return busStop.Name + " (" + to_string((double)busStop.Latitude / 10000.0) + ", "
                        + to_string((double)busStop.Longitude / 10000.0) + ")";
}


}
