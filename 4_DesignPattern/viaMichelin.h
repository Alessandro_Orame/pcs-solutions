#ifndef VIAMICHELIN_H
#define VIAMICHELIN_H

#include <iostream>
#include <exception>

#include "bus.h"
#include <vector>

using namespace std;
using namespace BusLibrary;

namespace ViaMichelinLibrary {

  //BusStation implementa l'interfaccia IBusStation
  class BusStation : public IBusStation {

  private:
    string _busFilePath;
    int _numberBuses;
    vector<Bus> _buses;

  public:
      BusStation(const string& busFilePath);

      void Load();
      int NumberBuses() const  {return _numberBuses;} //RITORNA IL NUMERO DI BUS DELLA BUS STATION
      const Bus& GetBus(const int& idBus) const;
  };


  //MapData implementa l'interfaccia IMapData
  class MapData : public IMapData {
  private:
      string _mapFilePath;
      int _numberRoutes;
      int _numberStreets;
      int _numberBusStops;
      vector<Route> _routes;
      vector<Street> _streets;
      vector<BusStop> _busStops;
      vector<int> _streetsFrom;
      vector<int> _streetsTo;
      vector<vector<int>> _routeStreets;

  public:
      void Reset();
      MapData(const string& mapFilePath) { _mapFilePath = mapFilePath; }
      void Load();
      int NumberRoutes() const {return _numberRoutes;}       //ritorna il numero di percorsi della Mappa
      int NumberStreets() const { return _numberStreets;}    //ritorna il numero di stade della Mappa
      int NumberBusStops() const { return _numberBusStops; } //ritorna il numero di fermate bus della Mappa
      const Street& GetRouteStreet(const int& idRoute, const int& streetPosition) const;
      const Route& GetRoute(const int& idRoute) const;
      const Street& GetStreet(const int& idStreet) const;
      const BusStop& GetStreetFrom(const int& idStreet) const;
      const BusStop& GetStreetTo(const int& idStreet) const;
      const BusStop& GetBusStop(const int& idBusStop) const;
  };


  //RoutePlanner implementa l'interfaccia IRoutePlanner
  class RoutePlanner : public IRoutePlanner {

  private:
    const IMapData& _mapData;
    const IBusStation& _busStation;

  public:
      static int BusAverageSpeed;

    public:
      RoutePlanner(const IMapData& mapData,
                   const IBusStation& busStation) : _mapData(mapData), _busStation(busStation) { }

      int ComputeRouteTravelTime(const int& idRoute) const;
      int ComputeRouteCost(const int& idBus, const int& idRoute) const;
  };


  //MapViewer implementa l'interfaccia IMapViewer
  class MapViewer : public IMapViewer {

  private:
    const IMapData& _mapData;

  public:
      MapViewer(const IMapData& mapData)  : _mapData(mapData) { }
      string ViewRoute(const int& idRoute) const;
      string ViewStreet(const int& idStreet) const;
      string ViewBusStop(const int& idBusStop) const;
  };
}

#endif // VIAMICHELIN_H
