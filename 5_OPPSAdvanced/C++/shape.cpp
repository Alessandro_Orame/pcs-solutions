#include "shape.h"
#include <math.h>

namespace ShapeLibrary {


//COSTRUTTORE CLASSE POINT
  Point::Point(const double& x,
               const double& y) {
      X = x;
      Y = y;
  }


  //COSTRUTTORE COPIA CLASSE POINT
  Point::Point(const Point& point) {

      X = point.X;
      Y = point.Y;
  }


  //CALCOLA LA NORMA EUCLIDEA DI UN PUNTO
  double Point::ComputeNorm2() const {

      return sqrt((X*X)+(Y*Y));
  }


  //OPERATORE DI ASSEGNAZIONE PER LA CLASSE PUNTO
  void Point::operator=(const Point &point)
  {
      X = point.X;
      Y = point.Y;
  }


  //OPERATORE SOMMA TRA DUE PUNTI
  //l'output è un punto dato dalla somma del punto P chiamante (la funzione)
  //e il punto dato in input alla funzione
  Point Point::operator+(const Point& point) const
  {
      return Point(X + point.X, Y + point.Y);
  }


  //OPERATORE DIFFERENZA TRA DUE PUNTI
  //l'output è un punto dato dalla differenza del punto P chiamante (la funzione)
  //e il punto dato in input alla funzione
  Point Point::operator-(const Point& point) const
  {
      return Point(X - point.X, Y - point.Y);
  }


  //OPERATORE SOMMA TRA DUE PUNTI
  //l'output è salvato direttamente negli attributi del punto P chiamante la funzione
  Point&Point::operator+=(const Point& point)
  {
      X += point.X;
      Y += point.Y;
     return *this;  //ritorniamo un puntatore al punto che abbiamo modificato attraverso la funzione
  }


  //OPERATORE DIFFERENZA TRA DUE PUNTI
  //l'output è salvato direttamente negli attributi del punto P chiamante la funzione
  Point&Point::operator-=(const Point& point)
  {
      X -= point.X;
      Y -= point.Y;
     return *this;  //ritorniamo un puntatore al punto che abbiamo modificato attraverso la funzione
  }


  //COSTRUTTORE CLASSE ELLIPSE
  Ellipse::Ellipse(const Point& center,
                   const double& a,
                   const double& b) {
      _a = a;
      _b = b;
      _center= center;
  }


  //Metodo AddVertex: il punto in input è assegnato al centro dell'ellisse
  void Ellipse::AddVertex(const Point &point){
      _center= point;
  }


  //CALCOLO DEL PERIMETRO DI UN ELLISSE
  //Questo metodo verrà ereditato anche dalla classe Circle,
  //che ne farà uso per poter calcolare il perimetro di un cerchio
  double Ellipse::Perimeter() const {

      return 2*M_PI*sqrt(double(_a*_a+_b*_b)/2);
  }


  //COSTRUTTORE CLASSE CIRCLE
  //Il cerchio eredita la struttura dall'ellisse,
  //dunque per costruire un cerchio facciamo uso del costruttore dell'ellisse
  //facendo attenzione ad assegnare in modo coerente gli input al costruttore
  Circle::Circle(const Point &center,
                 const double &radius) : Ellipse (center, radius, radius) {}


  //COSTRUTTORE TRIANGOLO
  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
  }


  //Metodo AddVertex: aggiungiamo il punto dato dalla funzione al vettore di punti del triangolo
  void Triangle::AddVertex(const Point &point) {
      points.push_back(point);
  }


  //CALCOLO DEL PERIMETRO DI UN TRIANGOLO
  //Questo metodo verrà ereditato anche dalla classe TriangleEquilateral,
  //che ne farà uso per poter calcolare il perimetro di un triangolo equilatero
  double Triangle::Perimeter() const {

      double a = (points[0] - points[1]).ComputeNorm2();
      double b = (points[1] - points[2]).ComputeNorm2();
      double c = (points[2] - points[0]).ComputeNorm2();
      double perimeter = a+b+c;

      return perimeter;
  }


  //COSTRUTTORE CLASSE TRIANGLE EQUILATERAL
  //Il triangolo equilatero eredita la struttura dal triangolo, dunque per costruire un triangolo equilatero
  //facciamo uso del costruttore del triangolo, facendo attenzione ad assegnare in modo coerente gli input
  TriangleEquilateral::TriangleEquilateral(const Point& p1,const double& edge) : Triangle(p1, 
                                                                                          p1 + Point(edge, 0.0),
                                                                                          p1 + Point(edge / 2, (sqrt(3) / 2) * edge)){}


  //COSTRUTTORE CLASSE QUADRILATERAL
  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4){
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
      points.push_back(p4);
  }


  //CALCOLO DEL PERIMETRO DI UN QUADRILATERO
  //Questo metodo verrà ereditato anche dalla classe Rectangle e Square,
  //che ne faranno uso per poter calcolare risepttivamente il perimetro di un rettangolo e di un quadrato
  double Quadrilateral::Perimeter() const
  {
      double a = (points[1] - points[0]).ComputeNorm2();
      double b = (points[2] - points[1]).ComputeNorm2();
      double c = (points[3] - points[2]).ComputeNorm2();
      double d = (points[0] - points[3]).ComputeNorm2();
      return a + b + c + d;
  }


  //Metodo AddVertex: aggiungiamo il punto dato dalla funzione al vettore di punti del quadrilatero
  void Quadrilateral::AddVertex(const Point &p){
      points.push_back(p);
  }


  //COSTRUTTORE CLASSE RECTANGLE
  //Il rettangolo eredita la struttura dal quadrilatero, dunque per costruire un rettangolo
  //facciamo uso del costruttore del quadrilatero, facendo attenzione ad assegnare in modo coerente gli input
  Rectangle::Rectangle(const Point &p1,
                       const Point &p2,
                       const Point &p3,
                       const Point &p4) : Quadrilateral(p1, p2, p3, p4) {}


  //COSTRUTTORE CLASSE RECTANGLE
  //Similmente al metodo precedente, unica differenza sono gli input in ingresso al costruttore del rettangolo,
  //che useremo con coerenza per costruire un rettangolo a partire dal costruttore del quadrilatero
  Rectangle::Rectangle(const Point& p1,const double& base,const double& height) : Quadrilateral(p1,
                                                                                                p1+Point(base, 0),
                                                                                                p1+Point(base, height),
                                                                                                p1+Point(0, height)) {}


  //COSTRUTTORE CLASSE SQUARE
  //Il quadrato eredita la struttura dal rettangolo, dunque per costruire un quadrato
  //facciamo uso del costruttore del rettangolo, facendo attenzione ad assegnare in modo coerente gli input
  Square::Square(const Point &p1,const Point &p2, const Point &p3, const Point &p4) : Rectangle(p1,p2, p3, p4) {}


  //COSTRUTTORE CLASSE SQUARE
  //Diversi input rispetto al metodo precedente
  Square::Square(const Point& p1,
                 const double& edge) : Rectangle(p1, edge, edge) {}



}
