#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      Point() {}
      Point(const double& x,
            const double& y);
      Point(const Point& point);

      double ComputeNorm2() const;

      void operator=(const Point& point);
      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);


      //OPERATORE DI OUTPUT
      //Dato un punto, andiamo a stampare nello stream una riga descrittiva degli attributi del punto
      // esempio: "Point: x= 3.4 y=2.5"
      friend ostream& operator<<(ostream& stream, const Point& point){
          stream << "Point: x = " << point.X << " y = " << point.Y;
          return stream;
      };
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0;
      virtual void AddVertex(const Point& point) = 0;

      //OPERATORI DI CONFRONTO

      //ritorna True se il perimetro del poligono lhs è minore del perimetro di rhs
      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs) {return lhs.Perimeter() < rhs.Perimeter();};

      //ritorna True se il perimetro del poligono lhs è maggiore del perimetro di rhs
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){return lhs.Perimeter() > rhs.Perimeter();};

      //ritorna True se il perimetro del poligono lhs è minore  o uguale del perimetro di rhs
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){return lhs.Perimeter() <= rhs.Perimeter();};

      //ritorna True se il perimetro del poligono lhs è maggiore o uguale del perimetro di rhs
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){return lhs.Perimeter() >= rhs.Perimeter();};

  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse() { }
      Ellipse(const Point& center,
              const double& a,
              const double& b);
      virtual ~Ellipse() { }
      void AddVertex(const Point& point);
      double Perimeter() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle() : Ellipse() { }
      Circle(const Point& center,
             const double& radius);
      virtual ~Circle() { }
  };


  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Triangle() {};
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      void AddVertex(const Point& point);
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral() : Triangle() {}
      TriangleEquilateral(const Point& p1,
                          const double& edge);
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Quadrilateral() { }
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { }

      void AddVertex(const Point& p);

      double Perimeter() const;
  };

  class Rectangle : public Quadrilateral
  {
    public:
      Rectangle() : Quadrilateral() { }
      Rectangle(const Point& p1,
                const Point& p2,
                const Point& p3,
                const Point& p4);
      Rectangle(const Point& p1,
                const double& base,
                const double& height);
      virtual ~Rectangle() { }
  };

  class Square: public Rectangle
  {
    public:
      Square() : Rectangle() { }
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4);
      Square(const Point& p1,
             const double& edge);
      virtual ~Square() { }
  };
}

#endif // SHAPE_H
